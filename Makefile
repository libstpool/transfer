.PHONY:all clean

CFLAGS = -g -I. -D_FILE_OFFSET_BITS=64 -D_LARGE_FILE

all:transfer

transfer:transfer_file.o
	g++ $(LDFLAGS) -o$@ $^ -lpthread

%.o:%.cpp
	g++ $(CFLAGS) -c $^


clean:
	@rm -fr xftool merge_file.o

