#linux文件合并工具
----
因为大文件合并需要, 自己动手写了个大文件合并工具,有需要的可以直接编译使用

.命令:
----
	./transfer -o 输出文件[:偏移] [-a] [-f 输入文件[:偏移[:长度]],输入文件[:偏移[:长度]],...] [-c偏移[:长度]] [-l 日志级别] [-b 缓冲大小]

.选项:  参数                         描述
----
    -o 输出的文件[:偏移]         数据合并输出文件,可以指定从哪个偏移位置开始写入
	-a                           表示是否在输出文件末尾添加新数据,如果不指定,则会截断输出文件
	-f 输入文件[:偏移[:长度]]    制定文件从哪个偏移开始搬移数据, 搬移多少数据(多个文件使用,隔开)
	-c [偏移:长度]               注意:-c选项与参数之间无空格,这是从终端第几个字符开始读取数据，读取多少
	-l 日志级别	                 日志级别0-4,级别越小,打印日志越多 (默认为1）
	-b 设置传输缓冲大小          默认是4M

.范例:
----
   1. 将大文件1.dat 2.dat 3.dat 附加到文件all.dat, 其中3.dat从偏移1024开始,长度为10240,其它文件全部搬移合并
		
		./transfer -o all.dat -f 1.data,2.dat,3.dat:1024:10240 -a

		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:852:@setOutputStream::setOutputStream:fsWrite/'all.dat'/440120/0/fd:3
		2017-07-15 08-28-45 [ERROR] |7facca676740|transfer_file.cpp:701:@getFStream::open('1.data’) err:No such file or directory
		2017-07-15 08-28-45 [ERROR] |7facca676740|transfer_file.cpp:900:@addFileSource::addSource('1.data', 0, -1) = false
		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:888:@addSource::addSource(fsRead/'2.dat'/406696/0:406696/fd:4)
		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:888:@addSource::addSource(fsRead/'3.dat'/219960/1024:10240/fd:5)
		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:957:@transfer::start transfering ...
		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:880:@setTransferBuffer::setTransferBuffer:4096 k
		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:801:@transfer::stream(fsRead/'2.dat'/406696/0:406696/fd:4) is EOF. transfered 406696 bytes
		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:801:@transfer::stream(fsRead/'3.dat'/219960/1024:10240/fd:5) is EOF. transfered 10240 bytes
		2017-07-15 08-28-45 [INFO ] |7facca676740|transfer_file.cpp:970:@transfer::Done. 416936 bytes has been transfered to stream(fsWrite/'all.dat'/440120/0/fd:3)
   
   2. 使用管道,将命令cmd的信息从第100个字符开始提取长度为1024个输出到output.txt

       cmd|./transfer -o output.txt -c100:1024

